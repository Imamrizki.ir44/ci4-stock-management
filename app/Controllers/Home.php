<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
    {
        $data = [
            'title' => 'Stock Management System'
        ];

        return view('home', $data);
    }
}
